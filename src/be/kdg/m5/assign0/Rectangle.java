package be.kdg.m5.assign0;

/**
 * Author: derijkej
 */
public class Rectangle {

	private int x;
	private int y;
	private int width;
	private int height;

	public Rectangle(int x, int y, int width, int height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;

	}

	public Rectangle() {
		//this(1,1,1,1);
		System.out.println("blabla");
	}

	public void setX(int x) {
		this.x = x;
	}


	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int aNumber) {
		System.out.println("setting an int");
		width = aNumber;
	}

	public void setWidth(double aNumber) {
		System.out.println("setting a double");
		width = (int) aNumber;
	}

	public int getHeight() {
		return height;

	}

	public void setHeight(int height) {

		this.height = height;
		setWidth(height);
	}


//	public String toString() {
//		return String.format("Rectancle at position (%d,%d) with width %d and height %d", x, y, width, height);
//	}


	@Override
	public String toString() {
		return "Rectangle{" +
			"x=" + x +
			", y=" + y +
			", width=" + width +
			", height=" + height +
			'}';
	}

	int surface() {
		return width * height;
	}
}
